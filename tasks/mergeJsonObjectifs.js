import { readdirSync, statSync, readFileSync } from 'fs'
import { join } from 'path'

// Function to read all JSON files in a directory and merge them into one JSON object
export const mergeJsonObjectifs = function (directory) {
  const mergedJSON = []

  const readDirectory = (dir) => {
    const files = readdirSync(dir).sort()

    files.forEach(file => {
      console.log(file)
      const filePath = join(dir, file)
      const stats = statSync(filePath)

      if (stats.isDirectory()) {
        // Recursively read subdirectories
        readDirectory(filePath)
      } else {
        if (file.endsWith('.json')) {
          const fileData = readFileSync(filePath, 'utf8')
          const jsonData = JSON.parse(fileData)

          // Check if jsonData has the expected structure
          if (jsonData.annee && jsonData.theme && jsonData.sousTheme && jsonData.objectif) {
            let existingAnneObj = mergedJSON.find(item => item.nom === jsonData.annee)
            if (!existingAnneObj) {
              existingAnneObj = { nom: jsonData.annee, themes: [] }
              mergedJSON.push(existingAnneObj)
            }

            let existingThemeObj = existingAnneObj.themes.find(theme => theme.nom === jsonData.theme)
            if (!existingThemeObj) {
              existingThemeObj = { nom: jsonData.theme, sousThemes: [] }
              existingAnneObj.themes.push(existingThemeObj)
            }

            let existingSousThemeObj = existingThemeObj.sousThemes.find(sousTheme => sousTheme.nom === jsonData.sousTheme)
            if (!existingSousThemeObj) {
              existingSousThemeObj = { nom: jsonData.sousTheme, objectifs: [] }
              existingThemeObj.sousThemes.push(existingSousThemeObj)
            }

            existingSousThemeObj.objectifs.push(jsonData.objectif)
          } else {
            console.error(`Invalid JSON data in file: ${filePath}`)
          }
        }
      }
    })
  }

  // Start reading the directory
  readDirectory(directory)

  return mergedJSON
}
