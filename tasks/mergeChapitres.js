import { readdirSync, statSync, readFileSync } from 'fs'
import { join } from 'path'

// Function to read all JSON files in a directory and merge them into one JSON object
export const mergeJsonChapitres = function (directory) {
  const mergedJSON = []

  const readDirectory = (dir) => {
    const files = readdirSync(dir)

    files.forEach(file => {
      const filePath = join(dir, file)
      const stats = statSync(filePath)

      if (stats.isDirectory()) {
        // Recursively read subdirectories
        readDirectory(filePath)
      } else {
        if (file.endsWith('.json')) {
          const fileData = readFileSync(filePath, 'utf8')
          const jsonData = JSON.parse(fileData)

          // Check if jsonData has the expected structure
          if (jsonData.documents && jsonData.sequencesListe && jsonData.periode && jsonData.infos && jsonData.titre && jsonData.reference && jsonData.nom) {
            let existingNomObj = mergedJSON.find(item => item.nom === jsonData.nom)
            if (!existingNomObj) {
              existingNomObj = { nom: jsonData.nom, chapitres: [] }
              mergedJSON.push(existingNomObj)
            }
            existingNomObj.chapitres.push({ periode: jsonData.periode, infos: jsonData.infos, titre: jsonData.titre, reference: jsonData.reference, sequencesListe: jsonData.sequencesListe, documents: jsonData.documents })
          } else {
            console.error(`Invalid JSON data in file: ${filePath}`)
          }
        }
      }
    })
  }

  // Start reading the directory
  readDirectory(directory)
  return mergedJSON
}
