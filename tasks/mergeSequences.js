import { readdirSync, statSync, readFileSync } from 'fs'
import { join } from 'path'

// Function to read all JSON files in a directory and merge them into one JSON object
export const mergeJsonSequences = function (directory) {
  const mergedJSON = []

  const readDirectory = (dir) => {
    const files = readdirSync(dir)

    files.forEach(file => {
      const filePath = join(dir, file)
      const stats = statSync(filePath)

      if (stats.isDirectory()) {
        // Recursively read subdirectories
        readDirectory(filePath)
      } else {
        if (file.endsWith('.json')) {
          const fileData = readFileSync(filePath, 'utf8')
          const jsonData = JSON.parse(fileData)

          // Check if jsonData has the expected structure
          if (jsonData.nom && jsonData.titre && jsonData.periode && jsonData.objectifs) {
            let existingNomObj = mergedJSON.find(item => item.nom === jsonData.nom)
            if (!existingNomObj) {
              existingNomObj = { nom: jsonData.nom, sequences: [] }
              mergedJSON.push(existingNomObj)
            }

            existingNomObj.sequences.push({
              titre: jsonData.titre,
              periode: jsonData.periode,
              objectifs: jsonData.objectifs,
              referenceDonnee: jsonData.reference,
              prerequis: jsonData.prerequis,
              calculsMentaux: jsonData.calculsMentaux || [], // Include calculsMentaux field with default empty array if not present
              organisationCours: jsonData.organisationCours || '',
              activiteSup: jsonData.activiteSup || [],
              questionsFlash: jsonData.questionsFlash || [],
              slugEvalBrevet: jsonData.slugEvalBrevet || '',
              documents: jsonData.documents || []
            })
          } else {
            console.error(`Invalid JSON data in file: ${filePath}`)
          }
        }
      }
    })
  }

  // Start reading the directory
  readDirectory(directory)
  return mergedJSON
}
