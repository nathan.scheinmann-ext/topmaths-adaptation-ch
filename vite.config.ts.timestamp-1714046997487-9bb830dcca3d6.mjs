// vite.config.ts
import { defineConfig } from "file:///C:/Users/Nathan/Documents/topmaths-adaptation-ch/node_modules/.pnpm/vite@5.2.7_@types+node@20.12.3_sass@1.72.0/node_modules/vite/dist/node/index.js";
import { svelte } from "file:///C:/Users/Nathan/Documents/topmaths-adaptation-ch/node_modules/.pnpm/@sveltejs+vite-plugin-svelte@3.0.2_svelte@4.2.12_vite@5.2.7/node_modules/@sveltejs/vite-plugin-svelte/src/index.js";
var vite_config_default = defineConfig({
  base: "/",
  build: {
    target: ["es2020", "edge88", "firefox78", "chrome87", "safari14"],
    sourcemap: true
  },
  server: process.env.CI ? { port: 80, watch: null } : { port: 5174, host: "127.0.0.1" },
  define: {
    APP_VERSION: JSON.stringify(process.env.npm_package_version)
  },
  plugins: [
    svelte({
      compilerOptions: {
        dev: true
      }
    })
  ]
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcudHMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCJDOlxcXFxVc2Vyc1xcXFxOYXRoYW5cXFxcRG9jdW1lbnRzXFxcXHRvcG1hdGhzLWFkYXB0YXRpb24tY2hcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfZmlsZW5hbWUgPSBcIkM6XFxcXFVzZXJzXFxcXE5hdGhhblxcXFxEb2N1bWVudHNcXFxcdG9wbWF0aHMtYWRhcHRhdGlvbi1jaFxcXFx2aXRlLmNvbmZpZy50c1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9pbXBvcnRfbWV0YV91cmwgPSBcImZpbGU6Ly8vQzovVXNlcnMvTmF0aGFuL0RvY3VtZW50cy90b3BtYXRocy1hZGFwdGF0aW9uLWNoL3ZpdGUuY29uZmlnLnRzXCI7aW1wb3J0IHsgZGVmaW5lQ29uZmlnIH0gZnJvbSAndml0ZSdcclxuaW1wb3J0IHsgc3ZlbHRlIH0gZnJvbSAnQHN2ZWx0ZWpzL3ZpdGUtcGx1Z2luLXN2ZWx0ZSdcclxuXHJcbi8vIGh0dHBzOi8vdml0ZWpzLmRldi9jb25maWcvXHJcbmV4cG9ydCBkZWZhdWx0IGRlZmluZUNvbmZpZyh7XHJcbiAgYmFzZTogJy8nLFxyXG4gIGJ1aWxkOiB7XHJcbiAgICB0YXJnZXQ6IFsnZXMyMDIwJywgJ2VkZ2U4OCcsICdmaXJlZm94NzgnLCAnY2hyb21lODcnLCAnc2FmYXJpMTQnXSxcclxuICAgIHNvdXJjZW1hcDogdHJ1ZVxyXG4gIH0sXHJcbiAgc2VydmVyOiAocHJvY2Vzcy5lbnYuQ0kgPyB7IHBvcnQ6IDgwLCB3YXRjaDogbnVsbCB9IDogeyBwb3J0OiA1MTczLCBob3N0OiAnMTI3LjAuMC4xJyB9KSxcclxuICBkZWZpbmU6IHtcclxuICAgIEFQUF9WRVJTSU9OOiBKU09OLnN0cmluZ2lmeShwcm9jZXNzLmVudi5ucG1fcGFja2FnZV92ZXJzaW9uKVxyXG4gIH0sXHJcbiAgcGx1Z2luczogW1xyXG4gICAgc3ZlbHRlKHtcclxuICAgICAgY29tcGlsZXJPcHRpb25zOiB7XHJcbiAgICAgICAgZGV2OiB0cnVlXHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgXVxyXG59KVxyXG4iXSwKICAibWFwcGluZ3MiOiAiO0FBQTRVLFNBQVMsb0JBQW9CO0FBQ3pXLFNBQVMsY0FBYztBQUd2QixJQUFPLHNCQUFRLGFBQWE7QUFBQSxFQUMxQixNQUFNO0FBQUEsRUFDTixPQUFPO0FBQUEsSUFDTCxRQUFRLENBQUMsVUFBVSxVQUFVLGFBQWEsWUFBWSxVQUFVO0FBQUEsSUFDaEUsV0FBVztBQUFBLEVBQ2I7QUFBQSxFQUNBLFFBQVMsUUFBUSxJQUFJLEtBQUssRUFBRSxNQUFNLElBQUksT0FBTyxLQUFLLElBQUksRUFBRSxNQUFNLE1BQU0sTUFBTSxZQUFZO0FBQUEsRUFDdEYsUUFBUTtBQUFBLElBQ04sYUFBYSxLQUFLLFVBQVUsUUFBUSxJQUFJLG1CQUFtQjtBQUFBLEVBQzdEO0FBQUEsRUFDQSxTQUFTO0FBQUEsSUFDUCxPQUFPO0FBQUEsTUFDTCxpQkFBaUI7QUFBQSxRQUNmLEtBQUs7QUFBQSxNQUNQO0FBQUEsSUFDRixDQUFDO0FBQUEsRUFDSDtBQUNGLENBQUM7IiwKICAibmFtZXMiOiBbXQp9Cg==
