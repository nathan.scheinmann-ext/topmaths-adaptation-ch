import { choice } from '../../../lib/outils/arrayOutils'
import { texteEnCouleur } from '../../../lib/outils/embellissements'
import { texNombre } from '../../../lib/outils/texNombre'
import Exercice from '../../deprecatedExercice.js'
import { randint, calculANePlusJamaisUtiliser } from '../../../modules/outils.js'

export const titre = 'Calculer astucieusement avec 100 ou 10'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCNum'
/**
 * Modèle d'exercice très simple pour la course aux nombres
 * @author Gilles Mora
 * Créé pendant l'été 2021
 * Référence can5C13
 * Date de publication
*/
export const uuid = 'ca4ce'
export const ref = 'can5C13'
export const refs = {
  'fr-fr': ['can5C13'],
  'fr-ch': []
}
export default function CalculAstucieux1 () {
  Exercice.call(this)
  this.typeExercice = 'simple'
  this.nbQuestions = 1
  this.tailleDiaporama = 2
  this.formatChampTexte = 'largeur15 inline'
  this.nouvelleVersion = function () {
    const a = randint(1, 9)
    const b = randint(1, 9, a)
    const c = randint(1, 9, [a, b])
    const d = calculANePlusJamaisUtiliser(a + b * 0.1 + c * 0.01)
    const e = calculANePlusJamaisUtiliser((2 * a + 1) / 2)
    const f = calculANePlusJamaisUtiliser(randint(1, 9) - 0.2)
    const g = randint(10, 90)
    switch (choice(['a', 'b', 'c', 'd', 'e', 'f', 'g'])) { //
      case 'a':
        this.question = `Calculer $4 \\cdot ${texNombre(d)}\\cdot 25$.`
        this.correction = `$4 \\cdot ${texNombre(d)}\\cdot 25 = ${texNombre(100 * d)}$`
        this.correction += texteEnCouleur(`<br> Mentalement : <br>
        $4 \\cdot ${texNombre(d)}\\cdot 25 =\\underbrace{4\\cdot 25}_{100}\\cdot ${texNombre(d)}= 100 \\cdot ${texNombre(d)} = ${texNombre(100 * d)}$ `)
        this.reponse = calculANePlusJamaisUtiliser(100 * d)
        break
      case 'b':
        this.question = `Calculer $2 \\cdot ${texNombre(d)}\\cdot 50$.`
        this.correction = `$2 \\cdot ${texNombre(d)}\\cdot 50 =  ${texNombre(100 * d)}$`
        this.reponse = calculANePlusJamaisUtiliser(100 * d)
        this.correction += texteEnCouleur(`<br> Mentalement : <br>
        $2 \\cdot ${texNombre(d)}\\cdot 50 = \\underbrace{2\\cdot 50}_{100} \\cdot ${texNombre(d)} = ${texNombre(100 * d)}$ `)
        break

      case 'c':
        this.question = `Calculer $25 \\cdot ${texNombre(d)}\\cdot 4$.`
        this.correction = `$25 \\cdot ${texNombre(d)}\\cdot 4 =  ${texNombre(100 * d)}$`
        this.reponse = calculANePlusJamaisUtiliser(100 * d)
        this.correction += texteEnCouleur(`<br> Mentalement : <br>
        $25 \\cdot ${texNombre(d)}\\cdot 4 = \\underbrace{4\\cdot 25}_{100} \\cdot ${texNombre(d)} = ${texNombre(100 * d)}$  `)
        break
      case 'd':
        this.question = `Calculer $2,5 \\cdot ${texNombre(d)}\\cdot 4$.`
        this.correction = `$2,5 \\cdot ${texNombre(d)}\\cdot 4 = 10 \\cdot ${texNombre(d)} = ${texNombre(10 * d)}$`
        this.reponse = calculANePlusJamaisUtiliser(10 * d)
        this.correction += texteEnCouleur(`<br> Mentalement : <br>
        $2,5 \\cdot ${texNombre(d)}\\cdot 4 =\\underbrace{2,5\\cdot 4}_{10} \\cdot ${texNombre(d)} = ${texNombre(10 * d)}$ `)
        break
      case 'e':
        this.question = `Calculer $${texNombre(e)} \\cdot ${texNombre(d)}+${texNombre(10 - e)}\\cdot ${texNombre(d)}$.`
        this.correction = `$${texNombre(e)} \\cdot ${texNombre(d)}+${texNombre(10 - e)}\\cdot ${texNombre(d)}=${texNombre(d)}\\cdot 10=${texNombre(10 * d)}$`
        this.reponse = calculANePlusJamaisUtiliser(10 * d)
        this.correction += texteEnCouleur(`<br> Mentalement : <br>
        On remarque une factorisation possible par le facteur commun $${texNombre(d)}$ qui permet de simplifier le calcul :<br>
        $${texNombre(e)} \\cdot ${texNombre(d)}+${texNombre(10 - e)}\\cdot ${texNombre(d)}=${texNombre(d)}\\cdot(\\underbrace{${texNombre(e)}+${texNombre(10 - e)}}_{10})=${texNombre(d)}\\cdot 10=${texNombre(10 * d)}$  `)
        break
      case 'f':
        this.question = `Calculer $${texNombre(f)} \\cdot ${texNombre(d)}+${texNombre(10 - f)}\\cdot ${texNombre(d)}$.`
        this.correction = `$${texNombre(f)} \\cdot ${texNombre(d)}+${texNombre(10 - f)}\\cdot ${texNombre(d)}=${texNombre(10 * d)}$`
        this.reponse = calculANePlusJamaisUtiliser(10 * d)
        this.correction += texteEnCouleur(`<br> Mentalement : <br>
        On remarque une factorisation possible par le facteur commun $${texNombre(d)}$ qui permet de simplifier le calcul :<br>
        $${texNombre(f)} \\cdot ${texNombre(d)}+${texNombre(10 - f)}\\cdot ${texNombre(d)}=${texNombre(d)}\\cdot(\\underbrace{${texNombre(f)}+${texNombre(10 - f)}}_{10})=${texNombre(d)}\\cdot 10=${texNombre(10 * d)}$. `)
        break
      case 'g':
        this.question = `Calculer $${texNombre(g)} \\cdot ${texNombre(d)}+${texNombre(100 - g)}\\cdot ${texNombre(d)}$.`
        this.correction = `$${g} \\cdot ${texNombre(d)}+${texNombre(100 - g)}\\cdot ${texNombre(d)}=${texNombre(100 * d)}$`
        this.reponse = calculANePlusJamaisUtiliser(100 * d)
        this.correction += texteEnCouleur(`<br> Mentalement : <br>
        On remarque une factorisation possible par le facteur commun $${texNombre(d)}$ qui permet de simplifier le calcul :<br>
        $${g} \\cdot ${texNombre(d)}+${texNombre(100 - g)}\\cdot ${texNombre(d)}=${texNombre(d)}\\cdot(\\underbrace{${texNombre(g)}+${texNombre(100 - g)}}_{100})=${texNombre(d)}\\cdot 100=${texNombre(100 * d)}$  `)
        break
    }
    this.canEnonce = this.question// 'Compléter'
    this.canReponseACompleter = ''
  }
}
