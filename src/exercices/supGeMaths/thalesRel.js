import RelationDeThales from './_RelationDeThalesModif.js'
export const dateDePublication = '05/01/2023'
export const interactifReady = false
export const titre = 'Écrire une relation de Thalès'

/**
 * Relation de Thalès
 * @author Sébastien LOZANO
*/

export const uuid = 'cd6c4'
export const refs = {
  'fr-fr': ['test-11GM3-34GE'],
  'fr-ch': ['11GM3-34GE']
}
export default function RelationDeThales3e () {
  RelationDeThales.call(this)
  this.sup = 2
}
