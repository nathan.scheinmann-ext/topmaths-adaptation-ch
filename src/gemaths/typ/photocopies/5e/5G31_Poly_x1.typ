#import "../../preambule_photocopies.typ": * 
#import "../../preambule_sequence.typ": * 
#show: doc => photocopies(doc, paysage: false, nbCols: 1)
#implication(image("../../cours/objectifs/5e/5G31-1.png", height: 12em), image("../../cours/objectifs/5e/5G31-2.png", height: 12em))
#implication(image("../../cours/objectifs/5e/5G31-1.png", height: 12em), image("../../cours/objectifs/5e/5G31-3.png", height: 12em))
#implication(image("../../cours/objectifs/5e/5G31-2.png", height: 12em), image("../../cours/objectifs/5e/5G31-1.png", height: 12em))
#implication(image("../../cours/objectifs/5e/5G31-3.png", height: 12em), image("../../cours/objectifs/5e/5G31-1.png", height: 12em))
