#import "../../preambule_photocopies.typ": * 
#show: doc => photocopies(doc, paysage: false, nbCols: 2)

#for i in range(6) {
  block(breakable: false)[
    Compléter le tableau en mettant Oui (O) ou Non (N).

    #table(columns: 6)[
    ... est divisible][par 2][par 3][par 5][par 9][par 10][
    3 330            ][     ][     ][     ][     ][      ][
    26               ][     ][     ][     ][     ][      ][
    597              ][     ][     ][     ][     ][      ][
    2 151            ][     ][     ][     ][     ][      ][
    73               ][     ][     ][     ][     ][      ][
    2 286            ][     ][     ][     ][     ][      ][
    130              ][     ][     ][     ][     ][      ][
    4 245            ][     ][     ][     ][     ][      ][
    535              ][     ][     ][     ][     ][      ][
    114              ][     ][     ][     ][     ][      ]
  ]
  v(2em)
}

