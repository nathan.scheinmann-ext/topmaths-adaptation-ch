#import "../../preambule_presentation.typ": *
#import "../../preambule_sequence.typ": *
#show: doc => presentation(doc, titre: "5N42 : Reconnaître les nombres premiers inférieurs ou égaux à 30")
#show: doc => normal(doc)
#slide()[
  #titrePrincipal()[Séquence 13 : Arithmétique 2]

  #definition()[
    Un #motDefini()[nombre premier] est un nombre qui possède exactement deux diviseurs : 1 et lui-même.
  ]

  #exemples()[
    Les dix premiers nombres premiers sont : 2 ; 3 ; 5 ; 7 ; 11 ; 13 ; 17 ; 19 ; 23 ; 29
  ]
]
