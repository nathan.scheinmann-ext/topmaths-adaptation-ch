#import "../../preambule_presentation.typ": *
#import "../../preambule_sequence.typ": *
#show: doc => presentation(doc, titre: "5G11 : Utiliser le fait que la symétrie centrale conserve le parallélisme, les longueurs et les angles")
#show: doc => normal(doc)

#slide()[
  #proprietes()[
  La symétrie (axiale ou centrale) conserve :
  - le parallélisme ;
  - les longueurs ;
  - les angles.
]

#exemple()[
  #image("../../cours/objectifs/5e/5G11-1.png", height: 4em)
]
]