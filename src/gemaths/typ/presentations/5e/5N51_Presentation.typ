#import "../../preambule_presentation.typ": *
#import "../../preambule_sequence.typ": *
#show: doc => presentation(doc, titre: "5N51 : Réduire une expression littérale")
#show: doc => normal(doc)

#slide()[
  #propriete()[
    $a$ et $b$ désignent des nombres.\
    $a #vert($x$) + b #vert($x$) = (a + b)#vert($x$)$
  ]
  #exemples()[
    $A = 2 times a = 2a$\
    $B = 2x + 3x = 5x$
  ]
]
