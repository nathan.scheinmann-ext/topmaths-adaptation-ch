#import "../../preambule_presentation.typ": *
#import "../../preambule_sequence.typ": *
#show: doc => presentation(doc, titre: "5G32 : Connaître et utiliser la somme des angles d'un triangle")
#show: doc => normal(doc)

#slide()[
  #titrePrincipal()[Séquence 14 : Somme des angles d'un triangle]
  #propriete()[
    La somme des trois angles d’un triangle fait (toujours) 180 °.
  ]
  #aColler(align(center, image("5G32-1.png", height: 6em)), 6em, watermark: "Coller son triangle")
]
