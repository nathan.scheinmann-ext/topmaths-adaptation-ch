#import "../../preambule_presentation.typ": *
#import "../../preambule_sequence.typ": *
#show: doc => presentation(doc, titre: "6G40 : Connaître le vocabulaire et les notations des angles")

#slide()[
  #uncover((2, 3))[#titrePrincipal("Séquence 10 : Angles")]
  #uncover((2, 3))[#underline("Exemple :")]
  #align(center)[
    #only((1, 2))[
      #image("6G40_Presentation-1.png", height: 70%)
    ]
    #only(3)[
      #image("6G40_Presentation-2.png", height: 70%)
    ]
  ]
]
