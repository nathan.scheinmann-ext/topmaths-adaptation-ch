#remarque()[
  Comme une aire s’exprime en #rouge()[$#normal("cm")^2$], #rouge()[$#normal("dm")^2$], #rouge()[$#normal(" m ")^2$], etc. il y a $2$ colonnes pour chaque unité dans le tableau de conversion.
]

#exemple()[
  #tablex(
    columns: (1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr, 1fr),
    inset: 0.6em,
    align: center + horizon,
    colspanx(2, rowspanx(2)[$"km"^2$]), (), colspanx(2)[$"hm"^2$], (), colspanx(2)[$"dam"^2$], (), colspanx(2)[$" m "^2$], (), colspanx(2, rowspanx(2)[$"dm"^2$]), (), colspanx(2, rowspanx(2)[$"cm"^2$]), (), colspanx(2, rowspanx(2)[$"mm"^2$]), (),
    (), (), colspanx(2)[$"hectare"$], (), colspanx(2)[$"are"$], (), colspanx(2)[$"centiare"$], (), (), (), (), (), (), (),
    [\ ], [], [], [], [], [], [], [], [], [], [], [], [], [],
    [\ ], [], [], [], [], [], [], [], [], [], [], [], [], []
  )
]
