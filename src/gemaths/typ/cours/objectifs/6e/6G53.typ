#propriete()[
  La symétrie axiale #rouge()[conserve] le #rouge()[parallélisme], les #rouge()[longueurs], et les #rouge()[angles].
]

#exemple()[
  #place(right, image("6G53-1.png", width: 39%), dx:20%, dy:-2em)
  #block(width: 80%)[
    Les triangles $"ABC"$ et $"A'BC'"$ ci-contre sont symétriques par rapport à la droite $(d)$.
    - Les droites $("AB")$ et $("KL")$ sont parallèles, leurs symétriques les droites $("A’B")$ et $("K’L’")$ sont donc aussi parallèles
    - Les segments $"AB"$ et $"A’B"$ sont symétriques, ils ont donc la même longueur
    - L’angle $hat("ABC")$ mesure $90 degree$, son symétrique l’angle $hat("A'BC'")$ mesure donc aussi $90 degree$
  ]
]
