#definition()[
  Un #motDefini()[angle] est une portion de plan mesurée par deux demi-droites de même origine.\
  L'origine de ces deux demi-droites est le #motDefini()[sommet] de l'angle.\
  Les deux demi-droites sont les #motDefini()[côtés] de l'angle.
]

#regle(titre: "Notation")[
  Un angle se nomme avec #vert()[trois lettres] où #rouge()[la lettre du milieu] correspond au #rouge()[sommet de l'angle].
]

#exemple()[
  #image("6G40-1.png")
]