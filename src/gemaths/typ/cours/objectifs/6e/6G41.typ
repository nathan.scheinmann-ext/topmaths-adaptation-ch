== Le degré
#definition()[
  Un angle plat peut être partagé en 180 parties égales.\
  Un #motDefini()[degré] (noté 1°) est la mesure de chacune de ces parties.
]

== Nature d'un angle

#vocabulaire()[
  #align(horizon + center, table(columns: 6)[][#image("6G41-1.png")][#image("6G41-2.png")][#image("6G41-3.png")][#image("6G41-4.png")][#image("6G41-5.png")][Angle][nul][aigu][droit][obtus][plat][Mesure][égale à 0°][comprise entre 0° et 90°][égale à 90°][comprise entre 90° et 180°][égale à 180°])
]
