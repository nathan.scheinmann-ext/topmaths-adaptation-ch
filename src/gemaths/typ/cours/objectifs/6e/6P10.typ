#definitions()[
  Deux grandeurs (longueur, prix, masse, durée etc.) sont #motDefini()[proportionnelles] si les valeurs de l’une s’obtiennent en multipliant les valeurs de l’autre par un même nombre appelé #motDefini()[coefficient de proportionnalité].
]

#exemple(titre: "Exemple 1")[
  Des letchis sont vendus à 3€ le kg. Le prix des letchis est-il proportionnel à leur masse ?\
  #normal()[
    On peut passer de la masse de letchis en kg à leur prix en € en multipliant par 3.\
    Le prix des letchis est donc proportionnel à leur masse et le coefficient de proportionnalité est 3.
  ]
]

#exemple(titre: "Exemple 2")[
  2 sachets de bonbons coûtent 5€ tandis que 3 sachets coûtent 6€.\
  Le prix est-il proportionnel au nombre de sachets ?\
  #normal()[
    5 ÷ 2 = 2,5\
    6 ÷ 3 = 2\
    On n’obtient pas le même résultat, le prix n’est donc pas proportionnel au nombre de sachets.
  ]
]
