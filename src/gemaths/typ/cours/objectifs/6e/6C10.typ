Connaître les tables de multiplication est extrêmement important pour réussir en mathématiques.\
Un élève qui ne les connaît pas aura beaucoup de mal avec la moitié du programme alors il est très important de s'entraîner jusqu'à les connaître au moins jusqu'à 9.
#v(1em)
Pour pouvoir commencer à les apprendre, il faut déjà les avoir.
#block(breakable: false)[
  Des tables de multiplications sont disponibles sur la page #link("https://topmaths.fr/?v=telechargements")[Téléchargements] de gemaths. Vous pouvez aussi les télécharger directement via ce QRCode :
  #v(1em)
  #align(center)[
    #image("6C10-1.png", width: 170pt)
    #link("https://topmaths.fr/Tables_de_multiplication")[topmaths.fr/Tables_de_multiplication]
  ]
]
#block(breakable: false)[
  Une fois qu'on les a, il ne faut pas les apprendre l'une après l'autre comme une poésie mais essayer de se les rappeler dans le désordre.\
  Une fois que vous les connaissez à peu près, vous pouvez vous entraîner sur gemaths en cliquant sur "Lancer l'exercice" en bas de la page 6C10.
  #align(center)[
    #image("6C10-2.png", width: 170pt)
    #link("https://topmaths.fr/?v=objectif&ref=6C10")[topmaths.fr/?v=objectif&ref=6C10]
  ]
]
#block(breakable: false)[
  Si vous voulez ne réviser que certaines tables, vous pouvez paramétrer vous-même l'entraînement que vous voulez sur mathsmentales.\
  À gauche vous choisissez les tables qui vous intéressent et à droite vous pouvez cocher "Fin du diaporama : Correction" et cliquer sur "C'est parti !".\
  Si vous voulez pouvoir écrire la réponse comme sur gemaths, il faut aussi cocher "Répondre en ligne : Oui" avant de cliquer sur "C'est parti !".
  #align(center)[
    #image("6C10-3.png", width: 170pt)
    #link("https://mathsmentales.net/?u=6ND6")[mathsmentales.net/?u=6ND6]
  ]
]
#block(breakable: false)[
  Enfin, pour y arriver ça ne sert à rien de passer des heures dessus d'affilée, il vaut mieux en faire un petit peu (quelques minutes) chaque jour !\
  Pour ça, mettre un rappel sur son téléphone est une très bonne idée. Tous les jours au début, puis quand ça devient trop facile mettre tous les trois jours, puis une semaine, deux semaines,...
]