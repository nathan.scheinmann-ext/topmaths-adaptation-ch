#definitions()[
  Un #motDefini()[quadrilatère] est un polygone à quatre côtés.\
  Un #motDefini()[losange] est un quadrilatère dont les #noir()[quatre côtés ont la même longueur].
]

#exemple()[
  #place(right, image("6G22-1.png", height: 7em), dx:100%, dy:-2em)
  MARK est un quadrilatère\
  et ses quatre côtés sont de même longueur\
  donc c’est un losange.
]

#definition()[
  Un #motDefini()[rectangle] est un quadrilatère qui a #vert()[quatre angles droits].
]

#exemple()[
  #place(right, image("6G22-2.png", height: 7em), dx:235%, dy:-2em)
  ABCD est un quadrilatère\
  et il a quatre angles droits\
  donc c’est un rectangle.
]

#definition()[
  Un #motDefini()[carré] est un quadrilatère qui a #vert()[quatre angles droits] et #noir()[quatre côtés de même longueur].
]

#exemple()[
  #place(right, image("6G22-3.png", height: 7em), dx:90%, dy:-2em)
  ABCD est un quadrilatère,\
  il a quatre angles droits\
  et ses quatre côtés sont de même longueur,\
  c’est donc un carré
]

#propriete()[
  Un carré est à la fois un rectangle et un losange.
]

#remarque(titre: "Preuve")[
  Un carré est un quadrilatère qui a quatre angle droits, c'est donc un rectangle.
  Un carré est un quadrilatère qui a quatre côtés de même longueur, c'est donc un losange.
]

#remarque(titre: "Résumé")[
  #image("6G22-4.png")
]