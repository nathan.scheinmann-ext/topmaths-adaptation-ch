#methode()[
  Pour calculer un pourcentage d'une quantité :
  + On calcule $1 %$ de cette quantité
  + On multiplie par le nombre de pourcents désiré
]

#exemple()[
  Dans un quatre-quarts de $600 g$, il y a $25 %$ de beurre.\
  Quelle est la quantité de beurre contenue dans ce quatre quarts ?

  #set text(couleurPrincipale)
  $1 %$ de $600 g$ c'est $1/100$ de $600 g$.

  $(600 g)/100 = 6 g$ donc $1 %$ de $600 g$ c'est $6 g$.

  On veut $25 times 1 %$ donc on calcule :
  
  $25 times 6 g = 125 g$

  Ce quatre-quarts contient $125 g$ de beurre.
]
