#methode()[
  Mesurer l’angle ci-dessous :\
  #image("6G42-1.png", height: 8em)
  #v(-2em)
  #normal()[
    #align(horizon + center, grid(columns: 3, column-gutter: 20pt)[#image("6G42-2.png")][#image("6G42-3.png")][#image("6G42-4.png")][Étape 1 :\ On place le rapporteur sur le sommet de l'angle][Étape 2 :\ On aligne l'un des zéros du rapporteur avec l'un des côtés de l'angle (ici le zéro extérieur)][Étape 3 :\ On lit que le deuxième côté de l'angle passe par la graduation de 38°])
  ]
]

#remarque()[
  On aurait pu aussi aligner le zéro des graduations intérieures avec l'autre côté de l'angle et on aurait alors lu la mesure de l'angle sur les graduations intérieures (qui serait aussi 38°)
]