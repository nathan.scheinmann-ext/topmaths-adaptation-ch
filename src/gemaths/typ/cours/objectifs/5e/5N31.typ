#exemples()[
  \+ 7 - 11 = - 4\
  \- 3 + 8 = + 5
]

#propriete()[
  Si on change l’ordre des opérations dans un programme de calcul contenant des additions et des soustractions, on obtient un programme de calcul équivalent.
]

#exemples()[
  \+ 7 – 11 = – 11 + 7 = – 4\
  \– 3 + 8 = + 8 – 3 = + 5
]
