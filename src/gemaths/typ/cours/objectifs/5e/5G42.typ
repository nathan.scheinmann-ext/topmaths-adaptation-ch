#propriete()[
  Si un quadrilatère a ses diagonales qui se coupent en leur milieu, alors c'est un parallélogramme.
  #implication(image("5G42-1.png", height: 7em), "ABCD est un parallélogramme")
]

#propriete()[
  Si un quadrilatère non croisé a ses côtés opposés de même longueur, alors c'est un parallélogramme.
  #implication(image("5G42-2.png", height: 7em), "ABCD est un parallélogramme")
]

#propriete()[
  Si un quadrilatère non croisé a deux côtés parallèles et de même longueur, alors c'est un parallélogramme.
  #implication(image("5G42-3.png", height: 7em), "ABCD est un parallélogramme")
]

#propriete()[
  Si un quadrilatère non croisé a ses angles opposés de même mesure, alors c'est un parallélogramme.
  #implication(image("5G42-4.png", height: 7em), "ABCD est un parallélogramme")
]
