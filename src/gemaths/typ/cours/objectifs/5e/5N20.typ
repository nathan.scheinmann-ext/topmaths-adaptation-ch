#proprietes()[
  Une fraction ne change pas si on multiplie ou divise son numérateur et son dénominateur par un même nombre non nul (différent de zéro).\
  $a$, $b$ et #rouge()[$k$] désignent trois nombres relatifs avec $b ≠ 0$ et $#rouge()[k] ≠ 0$.\
  #box()[
    $ a/b = (a #rouge()[$times k$])/(b #rouge()[$times k$]) $
  ]
  #h(1em)
  #box(height: 1.5em)[et]
  #h(1em)
  #box()[
    $ a/b = (a #rouge()[$div k$])/(b #rouge()[$div k$]) $
  ]
]

#exemples()[
  #block()[
    $ 3/4 = (3 #rouge()[$times 5$])/(4 #rouge()[$times 5$]) = 15/20 $
  ]
  #block()[
    $ (-56)/64 = (-56 #rouge()[$div 8$])/(64 #rouge()[$div 8$]) = (-7)/8 $
  ]
]