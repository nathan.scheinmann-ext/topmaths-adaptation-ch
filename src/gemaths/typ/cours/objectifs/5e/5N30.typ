#definition()[
  On décide de considérer – 1, – 2, – 3 ... comme de nouveaux nombres.\
  Ils sont précédés d’un signe « – » et on les appelle "#motDefini()[nombres négatifs]".
]

#remarques()[
  #vert()[\– 5 + 5] – 1= – 1\
  Donc : #vert()[0] – 1= – 1

  #vert()[\+ 4 – 4] + 2 = + 2\
  Donc : #vert()[0] + 2 = + 2\
  Or, on sait que #rouge()[0 + 2] = #rouge()[2]\
  On peut donc en déduire que #rouge()[+2] = #rouge()[2]
]

#definitions()[
  Les nombres entiers peuvent donc être notés avec un signe +, on les appelle des #motDefini()[nombres positifs].\
  On a vu que – 8 + 8 = 0, que – 5 + 5 = 0, que + 4 – 4 = 0 etc.\
  On dit que – 8 est #rouge()[l’opposé] de + 8 et que + 8 est #rouge()[l’opposé] de – 8, ou encore que + 8 et – 8 sont #rouge()[opposés].\
  Les nombres positifs et négatifs sont appelés « #rouge()[nombres relatifs] », ils sont écrits avec un signe + ou – et un nombre que l’on appelle la #rouge()[valeur absolue].
]

#remarques()[
  - Le nombre 0 est le seul nombre à la fois positif et négatif.
  - Sur une droite graduée, un nombre et son opposé sont "symétriques" par rapport à 0.
]