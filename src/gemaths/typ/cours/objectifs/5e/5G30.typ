== Angles complémentaires, supplémentaires

#definitions()[
  #place(right, image("5G30-1.png", width: 15%), dy: -2em)
  Deux angles sont #motDefini()[complémentaires] si la somme de leurs mesures fait $90 °$.\ \
  Deux angles sont #motDefini()[supplémentaires] si la somme de leurs mesures fait $180 °$.
]

#remarques()[
  Deux angles sont complémentaires si quand on les assemble ils forment un angle droit.\
  Deux angles sont supplémentaires si quand on les assemble ils forment un angle plat (une droite).
]

== Angles opposés par le sommet

#definition()[
  #place(right, image("5G30-2.png", width: 20%), dy: -1em)
  Deux angles #motDefini()[opposés par le sommet] ont :
  - le même sommet
  - des côtés dans le prolongement de l'autre
]

#propriete()[
  Si deux angles sont opposés par le sommet, alors ils ont la même mesure.
]


== Angles alternes-internes

#definition()[
  #place(right, image("5G30-3.png", width: 20%), dy: -1em)
  Une droite ($d_3$) coupe les droites ($d_1$) et ($d_2$) en $A$ et $B$.\
  Deux angles #motDefini()[alternes-internes] :
  - ont pour sommets $A$ et $B$
  - sont situés de part de d'autre de ($d_3$) (alternes)
  - sont situés entre ($d_1$) et ($d_2$) (internes).
]

== Angles correspondants

#definition()[
  #place(right, image("5G30-4.png", width: 20%), dy: -1em)
  Une droite ($d_3$) coupe les droites ($d_1$) et ($d_2$) en $A$ et $B$.\
  Deux angles #motDefini()[correspondants] :
  - ont pour sommets $A$ et $B$
  - sont situés d'un même côté de ($d_3$)
  - occupent la même position par rapport aux droites ($d_1$) et ($d_2$).
]
