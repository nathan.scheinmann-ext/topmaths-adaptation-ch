#definition()[
  Un #motDefini()[nombre premier] est un nombre qui possède exactement deux diviseurs : 1 et lui-même.
]

#exemples()[
  - 5 n’est divisible que par 1 et 5, donc 5 est un nombre premier.
  - 4 n’est pas divisible que par 1 et 4, il est aussi divisible par 2 donc 6 n’est pas un nombre premier.
  - 1 n’est divisible que par 1, comme il n'a qu'un seul diviseur 1 n’est pas un nombre premier.
]

#exemples()[
Les dix premiers nombres premiers sont : 2 ; 3 ; 5 ; 7 ; 11 ; 13 ; 17 ; 19 ; 23 ; 29
]