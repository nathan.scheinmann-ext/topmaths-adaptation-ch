#propriete()[
  $a$ et $b$ désignent des nombres.\
  $a #vert($x$) + b #vert($x$) = (a + b)#vert($x$)$
]

#exemples()[
  $C = 2#vert($x$) + 3#vert($x$)$\
  $C = (2 + 3)#vert($x$)$\
  $C = 5#vert($x$)$

  $D = 4#vert($a$) + 7#rouge($b$) – 3#vert($a$) + 2#rouge($b$)$\
  $D = 4#vert($a$) – 3#vert($a$) + 7#rouge($b$) + 2#rouge($b$)$\
  $D = #vert($a$) + 9#rouge($b$)$
]