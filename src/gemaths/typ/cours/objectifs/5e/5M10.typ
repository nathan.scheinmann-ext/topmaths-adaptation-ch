##6M10

##6M12

##6M13

#definitions()[
  Une #motDefini()[#rouge()[hauteur]] d’un parallélogramme est la distance entre deux côtés opposés du parallélogramme.\
  Pour l’obtenir, on trace une perpendiculaire à un côté qui va servir de #vert()[base].
]

#exemples()[
  #grid(
    columns: 2,
    column-gutter: 2em,
    image("5M10-1.png"), image("5M10-2.png")
  )
]


#propriete()[
  L’aire d’un parallélogramme se calcule grâce à la formule $cal(A) = #vert()[base] times #rouge()[hauteur]$
]
