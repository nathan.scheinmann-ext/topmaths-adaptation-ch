#definitions()[
  On dit qu’un plan est #motDefini()[à l’échelle] si les distances sur le plan sont proportionnelles aux distances réelles.
  Dans ce cas, le quotient $"distance sur le plan"/"distance réele"$ est un coefficient de proportionnalité et est appelé #motDefini()[l’échelle du plan].
]

#remarque()[
  Lorsque le plan est à l’échelle, on est dans une situation de proportionnalité et on peut donc utiliser tous les outils qu’on connaît qui s’appliquent aux situations de proportionnalité.
]

#exemple()[
  Quelle est la distance réelle à vol d’oiseau (en ligne droite) entre Saint Denis et Sainte Marie ?
  #image("5P11-1.png")
  #place(right, dy:2pt, text(size: 0.7em,link("https://www.openstreetmap.org/copyright")[© OpenStreetMap contributors]))
]

L’échelle est de $1/200000$ donc 1 cm sur la carte correspond à 200 000 cm dans la réalité.

#table(columns: 3)[
Distance sur la carte (en cm)][#align(center, "1")][6,2#flecheProportionnalite(200000)][
Distance dans la réalité (en cm)][200 000][?]

$6,2 times 200 000 = 1 240 000 "cm" = 12,4 "km"$\
La distance réelle entre Saint Denis et Sainte Marie à vol d’oiseau est 12,4 km.