#definition()[
  Une #motDefini()[expression littérale] est une expression (un calcul) dans laquelle un ou plusieurs nombres sont désignés par des lettres.
]

#exemple()[
  La formule qui permet de calculer le périmètre d’un cercle ($P = 2 times pi times r$) est une expression littérale
]

#regle(titre: "Règle d’écriture")[
  Dans une expression littérale, on peut supprimer le signe $times$ lorsqu'il est placé devant une lettre ou une parenthèse.
]

#exemples()[
  On veut simplifier l'écriture des expressions suivantes :\
  $A = 2 times a$\
  $A = 2 #vert($times$) a$ ← on peut supprimer le #vert($times$) devant une lettre\
  $A = 2a$

  $B = a times 3$\
  $B = a #rouge($times$) 3$ ← le #rouge($times$) n’est pas devant une lettre ou une parenthèse, on ne peut pas le supprimer !\
  $B = 3 #vert($times$) a$\
  $B = 3a$

  $P = 2 #vert($times$) π #vert($times$) r$\
  $P = 2 π r$
]

#regle(titre: "Règles d’écriture")[
  $a$ désigne un nombre.\
  $a times a = a^2$ (se lit "a au carré")\
  $a times a times a = a^3$ (se lit "a au cube")
]

#remarque()[
  Attention à bien courber ses $x$ !\
  $2 x times x = 2 x^2$ mais $2 x x x = 2 x^3$ !
]