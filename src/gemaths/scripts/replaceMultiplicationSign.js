// replaceMultiplicationSign.js
import fs from 'fs'
import path from 'path'

// Function to replace multiplication sign (×) with LaTeX command (\cdot)
function replaceMultiplicationSign (filePath) {
  try {
    // Read the contents of the file
    let content = fs.readFileSync(filePath, 'utf8')

    // Replace multiplication sign (×) with LaTeX command (\cdot)
    content = content.replace(/\\times/g, '\\cdot')

    content = content.replace(/l'antécédent/g, 'la préimage')
    content = content.replace(/les antécédents/g, 'les préimages')
    content = content.replace(/un antécédent/g, 'une préimage')
    content = content.replace(/des antécédents/g, 'des préimages')
    content = content.replace(/antécédent/g, 'préimage')

    content = content.replace(/le coefficient directeur/g, 'la pente')
    content = content.replace(/un coefficient directeur/g, 'une pente')
    content = content.replace(/coefficients directeurs/g, 'les coefficients de la pente')

    // Write the modified content back to the file
    fs.writeFileSync(filePath, content, 'utf8')
    console.log(`Replacement complete for file: ${filePath}`)
  } catch (error) {
    console.error(`Error replacing multiplication sign in file: ${filePath}`, error)
  }
}

// Function to recursively traverse directory and apply replacement to each file
function traverseDirectory (directoryPath) {
  // Get list of files and subdirectories in the directory
  const items = fs.readdirSync(directoryPath)

  // Iterate over each item
  items.forEach(item => {
    // Construct full path of the item
    const itemPath = path.join(directoryPath, item)

    // Check if the item is a directory
    if (fs.statSync(itemPath).isDirectory()) {
      // If it's a directory, recursively traverse it
      traverseDirectory(itemPath)
    } else {
      // If it's a file, apply the replacement
      replaceMultiplicationSign(itemPath)
    }
  })
}

// Start traversing the directory from the specified root directory
const rootDirectoryExos = 'src/exercices'
const rootDirectoryModules = 'src/modules'
traverseDirectory(rootDirectoryExos)
traverseDirectory(rootDirectoryModules)

